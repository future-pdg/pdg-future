#  Iteration retrospective Demo 1

This document represent the iteration retrospective for the first demo that took place on the 01.11.2018.

## Our retrospective

We didn't implement the issue "Define and design the showcase website" because we finally didn't think that it is important to define a website before finishing the Telegram bot.

All other issues were implemented.

## Teacher's retrospective

We need to think about test and about the delivery of the application.
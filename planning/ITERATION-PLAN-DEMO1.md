# Iteration plan Demo 1

This document represent the iteration plan for the first demo that will take place on the 01.11.2018.

## Telegram

1. Learn about telegram bots
2. Create the bot with BotFather

## Bot creation

1. The bot can be installed on any Telegram group/channel
2. Define all available command
3. Implement `/help` command
4. Implement `/list` command
5. Implement `/add` command
6. Create a logo
7. The bot can receive a new Task and respond with a static content (for testing purpose)

## Frontend / Backend

1. Design global architecture
2. Define and choose the SDK/Framework and the language to use for the project
3. Design the TODOKLM API
4. Design and define the REST API endpoints
5. Choose the DB System
6. DB Scheme
7. POC of REST API and REST Client API
8. Define and design the showcase website


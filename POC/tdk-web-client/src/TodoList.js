import React, { Component } from 'react';
import axios from 'axios';
import TodoItems from './TodoItems'
import { InputGroup, InputGroupAddon, Input, Button } from 'reactstrap';

const DEFAULT_CHAT = {
  chatId: 153645457,
  chatType: "private",
  userId: 153645457,
  author: "Kikirikou"
}

class TodoList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      items: [],
      isLoading: true,
      errors: false,
    }

    this.addItem = this.addItem.bind(this);
  }

  componentDidMount() {
    this.setState({isLoading: true});

    // Get tasks from the API every 3 seconds
    setInterval(() => {
      axios.get('/tasks', { params: {
        chat_id: DEFAULT_CHAT.chatId,
        chat_type: DEFAULT_CHAT.chatType,
        user_id: DEFAULT_CHAT.userId
      }}).then((response) => {
        const items = response.data.map((d) => {
          console.log(d.deadline);
          return {
            id: d.id,
            title: d.description,
            author: d.created_by.firstname + " " + d.created_by.lastname,
            description: d.description,
            deadline: d.deadline
          }
        });
        this.setState({ items: items });
      })
      .catch((error) => {
        console.log(error);
      })

      this.setState({isLoading: false});
    }, 3000);

  }

  addItem(e) {
    if (this.inputItem !== "") {
      console.log("VAL= " + this.inputItem);
      var newItem = {
        title: this.inputItem.value,
        description: this.inputItem.value,
        created_by: {
          telegram_id: DEFAULT_CHAT.userId,
          firstname: "Mentor",
          lastname: "Reka"
        },
        chat: {
          telegram_id: DEFAULT_CHAT.chatId,
          type: DEFAULT_CHAT.chatType
        },
        created_at: Date.now(),
        author: DEFAULT_CHAT.author
      };

      axios.post('tasks', newItem )
        .then((response) => {
          console.log(response);
          this.setState((prevState) => {
            return {
              items: prevState.items.concat(newItem)
            }
          })
          this.inputItem = "";
        })
        .catch((error) => {
          console.log(error);
        });
    }
    console.log(this.state.items);
    e.preventDefault();
  }

  /* TODO: add a deletion method here
  deleteItem(e) {
    if (this.)
  }*/

  render() {
    const { items, isLoading } = this.state;

    if (isLoading) {
      return <p>Loading ...</p>;
    }

    return (
      <div className="tdk-main">
        <h1>TDK - TODOKLM</h1>
        <div className="tdk-form">
          <form onSubmit={this.addItem}>
            <InputGroup>
              <Input name="inputItem" innerRef={(a) => this.inputItem = a} placeholder="Enter a task" />
              <InputGroupAddon addonType="append"><Button color="success" type="submit">add</Button></InputGroupAddon>
            </InputGroup>
          </form>
        </div>
        <TodoItems entries={items}/>
      </div>
    );
  }
}

export default TodoList;

import React, { Component } from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';

class TodoItems extends Component {

    createTasks(item) {
        return <div key={item.id}>
                    <ListGroupItem>{item.title}
                    {item.deadline !== null && <div><strong>Date:</strong> <i>{new Date(item.deadline).toLocaleDateString("fr-FR")}</i></div>}
                    <div><strong>Author:</strong> <i>{item.author}</i></div>
                    </ListGroupItem>
                </div>
    }

    render() {
        var todoEntries = this.props.entries;
        var listItems = todoEntries.map(this.createTasks);
    
        return (
            <ListGroup>
                {listItems}
            </ListGroup>
        );
    }

    handleClick(e) {
        console.log("HELLO");

    }

}

export default TodoItems;
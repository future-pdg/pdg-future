## MongoDB installation and configuration on MacOS X

Procedure

Install with brew:

``brew install mongodb``

Start MongoDB without access control.

```mongod --port 27017 --dbpath /data/db```

Connect to the instance.

```mongo --port 27017```

Create the user administrator.

```
use tdk
db.createUser(
   {
     user: "tdk-admin",
     pwd: "tdk-admin",
     roles: [ {role:"readWrite", db: "tdk"}, {role: "dbAdmin", db: "tdk"}, {role: "dbAdmin", db: "admin"} ]
   }
);
use admin
db.createUser(
   {
     user: "tdk-admin",
     pwd: "tdk-admin",
     roles: [ {role:"readWrite", db: "tdk"}, {role: "dbAdmin", db: "tdk"}, {role: "dbAdmin", db: "admin"} ]
   }
);
```

Create the TDK database:

```
use tdk
```

Re-start the MongoDB instance with access control.

```mongod --auth --port 27017 --dbpath /data/db1```

Authenticate as the user administrator.

```mongo --port 27017 -u "tdk-admin" -p "tdk-admin" --authenticationDatabase "tdk"```

If you want to install MongoDB support use this plugin: https://github.com/dboissier/mongo4idea
package ch.heig.vd.tdk.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
public class TdkUser {

    @Id
    @JsonProperty("telegram_id")
    private int telegramId;

    @JsonProperty("firstname")
    private String firstName;

    @JsonProperty("lastname")
    private String lastName;

    @JsonProperty("username")
    private String userName;

    @JsonProperty("language_code")
    private String languageCode;

    @JsonProperty("is_bot")
    private Boolean isBot;

    @CreatedDate
    private String createdAt;

    public TdkUser() {

    }

    public TdkUser(int telegramId, String firstName, String lastName, String userName, String languageCode, Boolean isBot) {
        this.telegramId = telegramId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.languageCode = languageCode;
        this.isBot = isBot;
    }

    public int getTelegramId() {
        return telegramId;
    }

    public void setTelegramId(int telegramId) {
        this.telegramId = telegramId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Boolean getBot() {
        return isBot;
    }

    public void setBot(Boolean bot) {
        isBot = bot;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == TdkUser.class) {
            TdkUser user = (TdkUser)obj;
            return this.telegramId == user.telegramId;
        }
        return false;
    }

    @Override
    public String toString() {
        return "TdkUser {" +
                "telegramId=" + telegramId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", languageCode='" + languageCode + '\'' +
                ", isBot=" + isBot +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}

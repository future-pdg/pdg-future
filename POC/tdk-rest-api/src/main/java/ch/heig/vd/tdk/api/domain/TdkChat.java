package ch.heig.vd.tdk.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Document(collection = "chat")
public class TdkChat {
    @Id
    @JsonProperty("telegram_id")
    private Long telegramId;

    @JsonProperty("type")
    private String type;

    @JsonProperty("title")
    private String title;

    @CreatedDate
    private String createdAt;

    @JsonProperty("nb_task_created")
    private Integer nbTasksCreated = 0;

    public TdkChat() {
    }

    public TdkChat(Long telegramId, String type, String title) {
        this.telegramId = telegramId;
        this.type = type;
        this.title = title;
    }

    public Long getTelegramId() {
        return telegramId;
    }

    public void setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void incrementNbTasksCreated() { this.nbTasksCreated++; }

    public Integer getNbTasksCreated() {
        return nbTasksCreated;
    }

    public void setNbTasksCreated(Integer nbTasksCreated) {
        this.nbTasksCreated = nbTasksCreated;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == TdkChat.class) {
            TdkChat chat = (TdkChat)obj;
            return this.telegramId.equals(chat.telegramId);
        }
        return false;
    }

    @Override
    public String toString() {
        return "TdkChat{" +
                "telegramId=" + telegramId +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", nbTasksCreated=" + nbTasksCreated +
                '}';
    }
}

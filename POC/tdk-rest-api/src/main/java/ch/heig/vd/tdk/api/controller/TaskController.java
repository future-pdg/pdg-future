package ch.heig.vd.tdk.api.controller;

import ch.heig.vd.tdk.api.domain.Task;
import ch.heig.vd.tdk.api.domain.TdkChat;
import ch.heig.vd.tdk.api.domain.TdkUser;
import ch.heig.vd.tdk.api.repository.ChatRepository;
import ch.heig.vd.tdk.api.repository.TaskRepository;
import ch.heig.vd.tdk.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("tasks")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;

    @RequestMapping(method = { RequestMethod.GET }, produces = "application/json")
    public List<Task> listTasks(@RequestParam("chat_id") String chatId,
                                @RequestParam("chat_type") String chatType,
                                @RequestParam("user_id") String userId) {
        TdkChat chat = chatRepository.findByTelegramId(new Long(chatId));
        List<Task> tasks = taskRepository.findTaskByChatOrderByDeadlineDesc(chat);
        return tasks;
    }

    @RequestMapping(value = "/search", method = { RequestMethod.GET }, produces = "application/json")
    public List<Task> searchTasks(@RequestParam("chat_id") String chatId,
                                  @RequestParam("q") String term) {
        TdkChat chat = chatRepository.findByTelegramId(new Long(chatId));
        List<Task> tasks = taskRepository.findTaskByChatAndDescriptionContains(chat, term);
        return tasks;
    }

    @RequestMapping(method = { RequestMethod.POST }, produces = "application/json")
    public ResponseEntity<Task> createTask(@RequestBody Task task) {
        if (task != null) {
            TdkUser user = userRepository.save(task.getCreatedBy());
            TdkChat chat = chatRepository.save(task.getChat());
            task.setCreatedBy(user);
            task.setChat(chat);
            taskRepository.save(task);
        }
        return new ResponseEntity(task, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<Task> update(@PathVariable("id") String id, @RequestBody Task task) {
        taskRepository.save(task);
        return new ResponseEntity<Task>(task, HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public void deleteTask(@PathVariable @NotNull String id) {
        if (taskRepository.existsById(id))
            taskRepository.deleteById(id);
    }

    /**
     * TODO: security refactoring => should delete only all tasks related to the user asked that !
     */
    @RequestMapping(value="/clear", method = RequestMethod.DELETE)
    public void clearTasks() {
        taskRepository.deleteAll();
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET, produces = "application/json")
    public Task findById(@PathVariable @NotNull String id) {
        Task task = null;
        if (taskRepository.existsById(id)) {
            Optional<Task> optionalTask = taskRepository.findById(id);
            task = optionalTask.get();
        }
        return task;
    }
}

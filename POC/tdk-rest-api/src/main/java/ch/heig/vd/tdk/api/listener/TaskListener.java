package ch.heig.vd.tdk.api.listener;

import ch.heig.vd.tdk.api.domain.Task;
import ch.heig.vd.tdk.api.domain.TdkChat;
import ch.heig.vd.tdk.api.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;

/**
 * The TaskListener will care about the friendlyId of tasks and sub_tasks.
 * This concept should be completely transparent for developers and creating after the insert and the update of a resource.
 * We have two main scenarios:
 *     1) This is a new task:
 *         - We need to increment the counter nbTaskCreated on the Chat related to this task
 *         - Update the friendlyId task => T<X> were <X> is the new Id (which basicaly is the counter nbTasksCreated + 1)
 *     2) This is a new sub_task:
 *         - We increment the nbSubTasksCreated on the parent task.
 *         - We update the friendlyId subTask field => T<X>S<Y> where <Y> is the new Sub-Id (which basicaly is the counter of nbSubTasksCreated + 1)
 *
 *     Examples:
 *             - FriendlyId for a Task      => T42 where 42 is related to the chat
 *             - FriendlyId for a subTask   => T42S3 which mean that, this is the subTask 3 of task 42
 */
@Component
public class TaskListener extends AbstractMongoEventListener<Task>
{
    @Autowired
    private ChatRepository chatRepository;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Task> event) {
        Task task = event.getSource();
        System.out.println("OnBeforeConvert");
        if (task.getFriendlyId().isEmpty() || ! task.getFriendlyId().matches("^[Tt]{1}\\d*$")) {
            // Scenario 1
            System.out.println("NbTasksCreated: (before)" + task.getChat().getNbTasksCreated());
            TdkChat tdkChat = chatRepository.findByTelegramId(task.getChat().getTelegramId());
            tdkChat.setNbTasksCreated(tdkChat.getNbTasksCreated() + 1);

            task.setChat(tdkChat);

            System.out.println("NbTasksCreated (after): " + task.getChat().getNbTasksCreated());

            task.setFriendlyId("T" + task.getChat().getNbTasksCreated());
            System.out.println("FriendlyId: " + task.getFriendlyId());
        } else {
            for (Task subTask : task.getSubTasks()) {
                if (subTask.getFriendlyId().isEmpty() || ! subTask.getFriendlyId().matches("^[Tt]{1}\\d[Ss]{1}\\d*$")) {
                    System.out.println("Scenario 2");
                    // Scenario 2
                    task.incrementNbSubTasksCreated();
                    subTask.setFriendlyId(task.getFriendlyId() + "S" + task.getNbSubTasksCreated());
                }
            }
        }
        super.onBeforeConvert(event);
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<Task> event) {
        Task task = event.getSource();
        System.out.println("OnBeforeSave");
        super.onBeforeSave(event);
    }

    @Override
    public void onAfterSave(AfterSaveEvent<Task> event) {
        super.onAfterSave(event);
        System.out.println("OnAfterSave");
        System.out.println("Value after save : " + event.getSource().getFriendlyId());
    }
}

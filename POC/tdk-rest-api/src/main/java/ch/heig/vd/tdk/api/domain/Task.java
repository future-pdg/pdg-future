package ch.heig.vd.tdk.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;
import java.util.LinkedList;

@Document(collection = "task")
public class Task {

    @Id
    @JsonProperty("id")
    private String id;

    @JsonProperty("friendly_id")
    private String friendlyId = "";

    @JsonProperty("description")
    private String description;

    @DBRef
    @JsonProperty("created_by")
    private TdkUser createdBy;

    @DBRef
    @JsonProperty("chat")
    private TdkChat chat;

    @CreatedDate
    @JsonProperty("created_at")
    private Date createdAt;

    @LastModifiedDate
    @JsonProperty("modified_at")
    private  Date modifiedAt;

    @JsonProperty("deadline")
    private Date deadline;

    @JsonProperty("telegram_ref")
    private String telegramRef;

    @JsonProperty("sub_tasks")
    private LinkedList<Task> subTasks = new LinkedList<>();

    @JsonProperty("nb_sub_tasks_created")
    private Integer nbSubTasksCreated = 0;

    @JsonProperty("is_sub_task")
    private boolean isSubTask;

    @JsonProperty("parent_task_id")
    private String parentTaskId;

    public Task() {

    }

    public Task(String description, TdkUser createdBy, TdkChat chat) {
        this.description = description;
        this.createdBy = createdBy;
        this.chat = chat;
    }

    public Task(String description, TdkUser createdBy, TdkChat chat, Date deadline) {
        this.description = description;
        this.createdBy = createdBy;
        this.chat = chat;
        this.deadline = deadline;
    }

    public Task(String id, String description, TdkUser createdBy, TdkChat chat, Date deadline) {
        this.id = id;
        this.description = description;
        this.createdBy = createdBy;
        this.chat = chat;
        this.deadline = deadline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TdkUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(TdkUser createdBy) {
        this.createdBy = createdBy;
    }

    public TdkChat getChat() {
        return chat;
    }

    public void setChat(TdkChat chat) {
        this.chat = chat;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getTelegramRef() {
        return telegramRef;
    }

    public void setTelegramRef(String telegramRef) {
        this.telegramRef = telegramRef;
    }

    public boolean isSubTask() {
        return isSubTask;
    }

    public void setIsSubTask(boolean subTask) {
        isSubTask = subTask;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }


    /**
     * Create and generate a friendly id based on number of sub tasks created
     * The nbSubTasksCreated has the role of a global counter
     * @param subTask
     */
    public void addSubTask(Task subTask) {
        subTask.setFriendlyId(this.getFriendlyId() + "s" + this.nbSubTasksCreated + 1);
        this.nbSubTasksCreated++;
        subTasks.add(subTask);
    }

    public LinkedList<Task> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(LinkedList<Task> subTasks) {
        this.subTasks = subTasks;
    }

    public String getFriendlyId() {
        return friendlyId;
    }

    public void setFriendlyId(String friendlyId) {
        this.friendlyId = friendlyId;
    }

    public void incrementNbSubTasksCreated() {
        this.nbSubTasksCreated++;
    }
    public Integer getNbSubTasksCreated() {
        return nbSubTasksCreated;
    }

    public void setNbSubTasksCreated(Integer nbSubTasksCreated) {
        this.nbSubTasksCreated = nbSubTasksCreated;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == Task.class) {
            Task task = (Task)obj;
            if (this.deadline == null) {
                return this.description.equals(task.description) && this.createdBy.equals(task.createdBy)
                        && this.chat.equals(task.chat) && task.deadline == null;
            } else if (this.deadline != null) {
                return this.description.equals(task.description) && this.createdBy.equals(task.createdBy)
                        && this.chat.equals(task.chat) && this.deadline.equals(task.deadline);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", friendlyId='" + friendlyId + '\'' +
                ", description='" + description + '\'' +
                ", createdBy=" + createdBy +
                ", chat=" + chat +
                ", createdAt=" + createdAt +
                ", modifiedAt=" + modifiedAt +
                ", deadline=" + deadline +
                ", telegramRef='" + telegramRef + '\'' +
                ", subTasks=" + subTasks +
                ", nbSubTasksCreated=" + nbSubTasksCreated +
                '}';
    }

}

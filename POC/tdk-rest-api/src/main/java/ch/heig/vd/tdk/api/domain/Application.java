package ch.heig.vd.tdk.api.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Application {

    @Id
    private int Id;

    private String name;

    private String key;

    private String token;

    private String createdAt;

    private Boolean enable;

    public Application(){

    }

    public Application(String name, String key, String token, String createdAt, Boolean enable) {
        this.name = name;
        this.key = key;
        this.token = token;
        this.createdAt = createdAt;
        this.enable = enable;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        return "Application {" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", token='" + token + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", enable=" + enable +
                '}';
    }
}

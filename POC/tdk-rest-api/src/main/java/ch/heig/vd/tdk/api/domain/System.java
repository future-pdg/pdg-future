package ch.heig.vd.tdk.api.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "system")
public class System {

    @DBRef
    private List<Application> accesses;

    public System() {

    }

    @Override
    public String toString() {
        return "System {" +
                "accesses=" + accesses +
                '}';
    }
}

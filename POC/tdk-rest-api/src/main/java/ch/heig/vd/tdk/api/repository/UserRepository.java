package ch.heig.vd.tdk.api.repository;


import ch.heig.vd.tdk.api.domain.Task;
import ch.heig.vd.tdk.api.domain.TdkUser;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;

public interface UserRepository extends PagingAndSortingRepository<TdkUser, Serializable> {

}

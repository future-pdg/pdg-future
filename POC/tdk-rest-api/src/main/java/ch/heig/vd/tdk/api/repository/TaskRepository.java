package ch.heig.vd.tdk.api.repository;


import ch.heig.vd.tdk.api.domain.Task;
import ch.heig.vd.tdk.api.domain.TdkChat;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;

public interface TaskRepository extends PagingAndSortingRepository<Task, Serializable> {
    List<Task> findTaskByChat(TdkChat chat);
    List<Task> findTaskByChatOrderByDeadlineDesc(TdkChat chat);
    // TODO add OrderByDeadlineDesc support
    //List<Task> findTaskByChatAndDescriptionContainsOrderByDeadline(TdkChat chat, String searchTerm);
    List<Task> findTaskByChatAndDescriptionContains(TdkChat chat, String searchTerm);
    Task findByFriendlyIdAndChat(String friendlyId, TdkChat chat);
}

package ch.heig.vd.tdk.api.repository;


import ch.heig.vd.tdk.api.domain.Task;
import ch.heig.vd.tdk.api.domain.TdkChat;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;

public interface ChatRepository extends PagingAndSortingRepository<TdkChat, Serializable> {
    TdkChat findByTelegramId(Long telegramId);
}

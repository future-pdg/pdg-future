package ch.heig.vd.tdk.rest.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;


public class Task {

    @JsonProperty("id")
    @JsonIgnore
    private String id;

    @JsonProperty("description")
    private String description;

    @JsonProperty("created_by")
    private TdkUser createdBy;

    @JsonProperty("chat")
    private TdkChat tdkChat;

    @JsonProperty("created_at")
    private Date createdAt;

    @JsonProperty("modified_at")
    private  Date modifiedAt;

    @JsonProperty("deadline")
    private Date deadline;

    @JsonProperty("telegram_ref")
    private String telegramRef;

    @JsonProperty("sub_tasks")
    private LinkedList<Task> subTasks = new LinkedList<Task>();

    private Integer nbSubTasksCreated = 0;

    @JsonProperty("is_sub_task")
    private boolean isSubTask;

    @JsonProperty("parent_task_id")
    private String parentTaskId;

    public Task() {

    }

    public Task(String description, TdkUser createdBy, TdkChat tdkChat) {
        this.description = description;
        this.createdBy = createdBy;
        this.tdkChat = tdkChat;
    }

    public Task(String description, TdkUser createdBy, TdkChat tdkChat, Date deadline) {
        this.description = description;
        this.createdBy = createdBy;
        this.tdkChat = tdkChat;
        this.deadline = deadline;
    }

    public Task(String id, String description, TdkUser createdBy, TdkChat tdkChat, Date deadline) {
        this.id = id;
        this.description = description;
        this.createdBy = createdBy;
        this.tdkChat = tdkChat;
        this.deadline = deadline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TdkUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(TdkUser createdBy) {
        this.createdBy = createdBy;
    }

    public TdkChat getTdkChat() {
        return tdkChat;
    }

    public void setTdkChat(TdkChat tdkChat) {
        this.tdkChat = tdkChat;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getTelegramRef() {
        return telegramRef;
    }

    public void setTelegramRef(String telegramRef) {
        this.telegramRef = telegramRef;
    }

    public LinkedList<Task> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(LinkedList<Task> subTasks) {
        this.subTasks = subTasks;
    }

    public boolean isSubTask() {
        return isSubTask;
    }

    public void setIsSubTask(boolean subTask) {
        isSubTask = subTask;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == Task.class) {
            return this.id.equals(((Task)obj).id);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Task {" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", createdBy=" + createdBy +
                ", tdkChat=" + tdkChat +
                ", createdAt=" + createdAt +
                ", modifiedAt=" + modifiedAt +
                ", deadline=" + deadline +
                ", telegramRef='" + telegramRef + '\'' +
                '}';
    }
}

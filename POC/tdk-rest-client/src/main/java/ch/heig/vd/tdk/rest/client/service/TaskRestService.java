package ch.heig.vd.tdk.rest.client.service;

import ch.heig.vd.tdk.rest.client.config.AppConfig;
import ch.heig.vd.tdk.rest.client.dto.Task;
import ch.heig.vd.tdk.rest.client.dto.TdkChat;
import ch.heig.vd.tdk.rest.client.dto.TdkUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.*;

@Service
public class TaskRestService {

    @Autowired
    private AppConfig appConfig;

    private RestTemplate restTemplate;

    private final String TASK_ROOT_ENDPOINT = "http://127.0.0.1:8080/tasks";

    public TaskRestService() {
        this.restTemplate = new RestTemplate();
    }

    public List<Task> getTasks(TdkChat channel, TdkUser user) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(TASK_ROOT_ENDPOINT)
                .queryParam("chat_id", channel.getTelegramId())
                .queryParam("chat_type", channel.getType())
                .queryParam("user_id", user.getTelegramId());
        Task[] response  = restTemplate.getForObject(builder.toUriString(), Task[].class);
        return Arrays.asList(response);
    }

    public List<Task> searchTasks(TdkChat channel, TdkUser user, String term) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(TASK_ROOT_ENDPOINT + "/search")
                .queryParam("chat_id", channel.getTelegramId())
                .queryParam("chat_type", channel.getType())
                .queryParam("user_id", user.getTelegramId())
                .queryParam("q", term);
        Task[] response  = restTemplate.getForObject(builder.toUriString(), Task[].class);
        return Arrays.asList(response);
    }

    public Task createTask(Task task) {
        ResponseEntity<Task> response = restTemplate.postForEntity(TASK_ROOT_ENDPOINT, task, Task.class);
        return response.getBody();
    }

    public void updateTask(Task task) {
        String uri = TASK_ROOT_ENDPOINT + "/{id}";
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", task.getId());
        restTemplate.put(uri, task, map);
    }

    public void deleteTask(Task task) {
        String uri = TASK_ROOT_ENDPOINT + "/{id}";
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", task.getId());
        restTemplate.delete(uri, map);
    }

    public void clearTasks() {
        restTemplate.delete(TASK_ROOT_ENDPOINT + "/clear");
    }

    public Task findById(String id) {
        String uri = TASK_ROOT_ENDPOINT + "/{id}";
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", id);
        Task task = restTemplate.getForObject(uri, Task.class, map);
        return task;
    }
}

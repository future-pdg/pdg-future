package ch.heig.vd.tdk.rest.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TaskListDTO {

    @JsonProperty("tasks")
    private List<Task> tasks;

    public TaskListDTO(List<Task> tasks) {
        this.tasks = tasks;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}

package ch.heig.vd.tdk.rest.client.config;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource("classpath:/app.properties")
public class AppConfig {

    @Autowired
    Environment env;

    @Bean
    public Dotenv dotenv() {
        return Dotenv
                .configure()
                .ignoreIfMissing()
                .load();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public String getApiRootURI() {
        return this.dotenv().get("API_PROTOCOL") + this.dotenv().get("API_URL") + ":" + this.dotenv().get("API_PORT") + "/";
    }

    public String test() {
        return env.getProperty("API_ROOT_URL");
    }
}

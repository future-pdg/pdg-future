package ch.heig.vd.tdk.rest.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TdkChat {

    // TdkChat type
    public static final String USERCHATTYPE = "private";
    public static final String GROUPCHATTYPE = "group";
    public static final String CHANNELCHATTYPE = "channel";
    public static final String SUPERGROUPCHATTYPE = "supergroup";

    @JsonProperty("telegram_id")
    private Long telegramId;

    @JsonProperty("type")
    private String type;

    @JsonProperty("title")
    private String title;

    private String createdAt;

    @JsonProperty("nb_task_created")
    private Integer nbTasksCreated;

    public TdkChat() {
        this.nbTasksCreated = 0;
    }

    public TdkChat(Long telegramId, String type, String title) {
        this.telegramId = telegramId;
        this.type = type;
        this.title = title;
        this.nbTasksCreated = 0;
    }

    public Long getTelegramId() {
        return telegramId;
    }

    public void setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == TdkChat.class) {
            TdkChat chat = (TdkChat)obj;
            return this.telegramId.equals(chat.telegramId);
        }
        return false;
    }

    @Override
    public String toString() {
        return "TdkChat {" +
                "telegramId=" + telegramId +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}

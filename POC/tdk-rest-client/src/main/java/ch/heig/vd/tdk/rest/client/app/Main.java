package ch.heig.vd.tdk.rest.client.app;

import ch.heig.vd.tdk.rest.client.config.AppConfig;
import ch.heig.vd.tdk.rest.client.dto.TdkChat;
import ch.heig.vd.tdk.rest.client.dto.TdkUser;
import ch.heig.vd.tdk.rest.client.service.TaskRestService;
import ch.heig.vd.tdk.rest.client.dto.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@ComponentScan("ch.heig.vd.tdk.rest.client.*")
@Import(AppConfig.class)
public class Main {

    @Autowired
    private TaskRestService taskRestService;

    public static void main(String args[]) {
        TaskRestService taskRestService = new TaskRestService();

        TdkUser mraUser = new TdkUser(124062689, "Mentor", "Reka", "MRMENTOR", "fr-FR", false);
        TdkChat pdgGroup = new TdkChat(new Long(124062689), TdkChat.GROUPCHATTYPE, "Test");
        System.out.println("Result: " + taskRestService.createTask(new Task("test task", mraUser, pdgGroup)));
        for (Task task : taskRestService.getTasks(pdgGroup, mraUser)) {
            System.out.println(task);
        }
        taskRestService.searchTasks(pdgGroup, mraUser, "test");
    }

}
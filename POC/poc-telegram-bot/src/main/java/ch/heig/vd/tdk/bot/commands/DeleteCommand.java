/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : DeleteCommand.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of the /delete command

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/


package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import ch.heig.vd.tdk.bot.services.Emoji;
import ch.heig.vd.tdk.rest.client.dto.Task;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.LinkedList;

public class DeleteCommand extends Command  {

    /**
     * constructor
     * @param iCommandRegistry the commands handler
     */
    public DeleteCommand(ICommandRegistry iCommandRegistry) {
        super("delete", "Delete a task", iCommandRegistry);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        super.execute(absSender, user, chat, arguments);

        if (arguments.length == 1) {
            String id = arguments[0];
            if (id.startsWith("#"))  id = id.substring(1);
            Task task = taskRestService.findById(id);
            if (task != null) {
                taskRestService.deleteTask(task);
                if (task.isSubTask()) {
                    Task parent = taskRestService.findById(task.getParentTaskId());
                    LinkedList<Task> subTasks = parent.getSubTasks();
                    subTasks.remove(task);
                    taskRestService.updateTask(parent);
                }
                responseMessage.setText(Util.displayTask(task) + " <b>-></b> " + Emoji.TRASH);
            } else {
                responseMessage.setText("Task not found. Please give a correct task id." + Emoji.ANGRY_FACE);
            }
        } else {
            responseMessage.setText("Your command is not correct" + Emoji.ANGRY_FACE);
        }
        Util.sendMessageToChat(absSender, responseMessage);
    }
}

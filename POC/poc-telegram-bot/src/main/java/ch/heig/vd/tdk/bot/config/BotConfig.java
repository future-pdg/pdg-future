/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : BotConfig.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implement the bot configuration

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.config;

import io.github.cdimascio.dotenv.Dotenv;

public class BotConfig {
    private static Dotenv env;

    private static BotConfig instance;

    private BotConfig() {
        env = Dotenv.configure()
                .ignoreIfMalformed()
                .ignoreIfMissing()
                .load();
    }

    public static BotConfig getInstance() {
        if (instance == null) {
            instance = new BotConfig();
        }
        return instance;
    }

    public String getToken() {
        return env.get("TELEGRAM_PRIVATE_TOKEN");
    }

    public String getBotName() {
        return env.get("BOT_NAME");
    }

}

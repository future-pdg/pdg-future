/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : ClearCommand.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of the /clear command

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import ch.heig.vd.tdk.bot.services.Emoji;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.*;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.bots.AbsSender;
import java.util.ArrayList;
import java.util.List;

public class ClearCommand extends Command {

    private AbsSender absSender;

    /**
     * constructor
     * @param iCommandRegistry the commands handler
     */
    public ClearCommand (ICommandRegistry iCommandRegistry) {
        super("clear", "Delete all existing tasks.", iCommandRegistry);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        super.execute(absSender, user, chat, arguments);
        this.absSender = absSender;

        if (taskRestService.getTasks(tdkChat, tdkUser).isEmpty()) {
            responseMessage.setText("There is no existing task!");
        } else {
            InlineKeyboardMarkup inlineKeyboard = new InlineKeyboardMarkup();
            List<InlineKeyboardButton> row = new ArrayList<>();
            row.add(new InlineKeyboardButton().setText("YES").setCallbackData(Util.CLEAR_YES));
            row.add(new InlineKeyboardButton().setText("NO").setCallbackData(Util.CLEAR_NO));
            List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
            keyboard.add(row);

            inlineKeyboard.setKeyboard(keyboard);
            responseMessage.setText(Emoji.WARNING + " Are you sure?");
            responseMessage.setReplyMarkup(inlineKeyboard);
        }
        Util.sendMessageToChat(absSender, responseMessage);
}

    /**
     * clear all tasks (call the REST Client)
     */
    public void clearTasks() {
        taskRestService.clearTasks();
    }

    /**
     * handle the user response of the inline keyboard
     * @param update the update corresponding the user response
     */
    public void handleClearResponse(Update update) {
        Message message = update.getCallbackQuery().getMessage();
        String response = update.getCallbackQuery().getData();
        EditMessageText editMessage = new EditMessageText();
        editMessage.setChatId(message.getChatId()).setMessageId(message.getMessageId());
        if (response.equals(Util.CLEAR_YES)) {
            clearTasks();
            editMessage.setText(Emoji.BOMB + " ... " + Emoji.EXPLOSION);
        } else if (response.equals(Util.CLEAR_NO)) {
            editMessage.setText(Emoji.SMILING_FACE_WITH_OPEN_MOUTH_AND_COLD_SWEAT.toString());
        }

        Util.sendMessageToChat(absSender, editMessage);
    }
}
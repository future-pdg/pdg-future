/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : CommandsHandler.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation the commands handler

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.handlers;

import ch.heig.vd.tdk.bot.commands.*;
import ch.heig.vd.tdk.bot.config.BotConfig;
import ch.heig.vd.tdk.bot.services.Emoji;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class CommandsHandler extends TelegramLongPollingCommandBot {

    private ClearCommand clearCommand;

    /**
     * constructor
     * @param botUsername the name of the bot
     */
    public CommandsHandler(String botUsername) {
        super(botUsername);

        HelpCommand helpCommand = new HelpCommand(this);
        clearCommand = new ClearCommand(this);
        register(helpCommand);
        register(new AddCommand(this));
        register(new AddSubCommand(this));
        register(clearCommand);
        register(new DeleteCommand(this));
        register(new ListCommand(this));
        register(new SettingsCommand(this));
        register(new UpdateCommand(this));

        registerDefaultAction((absSender, message) -> {
            SendMessage commandUnknownMessage = new SendMessage();
            commandUnknownMessage.setChatId(message.getChatId());
            commandUnknownMessage.setText("The command '" + message.getText() + "' is not known by this bot. Here comes some help " + Emoji.AMBULANCE);
            try {
                absSender.execute(commandUnknownMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
            helpCommand.execute(absSender, message.getFrom(), message.getChat(), new String[] {});
        });
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        if (update.hasCallbackQuery()) {
            if (update.getCallbackQuery().getData().startsWith("c")) clearCommand.handleClearResponse(update);
        }
    }

    @Override
    public String getBotToken() {
        return BotConfig.getInstance().getToken();
    }
}

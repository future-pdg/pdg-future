/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe 2018 (PDG) - TODOKLM
 File        : UpdateCommand.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of the /update command

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import ch.heig.vd.tdk.bot.services.Emoji;
import ch.heig.vd.tdk.rest.client.dto.Task;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;

public class UpdateCommand extends Command {

    private Date eraseDate;

    /**
     * constuctor
     * @param iCommandRegistry the commands handler
     */
    public UpdateCommand(ICommandRegistry iCommandRegistry) {
        super("update", "Update a task", iCommandRegistry);
        try {
            eraseDate = new SimpleDateFormat("dd.MM.yyyy").parse("00.00.0000");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        super.execute(absSender, user, chat, arguments);

        if (arguments.length < 2) {
            responseMessage.setText("There is too few arguments.");
        } else {
            boolean toEraseDate = false;
            String id = arguments[0];
            if (id.startsWith("#"))  id = id.substring(1);
            Task task = taskRestService.findById(id);
            if (task != null) {
                Object[] array = Util.getDescriptionAndDeadline(Arrays.copyOfRange(arguments, 1, arguments.length));
                String description = (String) array[0];
                Date deadline = (Date) array[1];

                //if date is 00.00.0000 delete it
                if (deadline != null && deadline.equals(eraseDate)) {
                    toEraseDate = true;
                    deadline = null;
                }

                try {
                    if (deadline != null || toEraseDate) task.setDeadline(deadline);
                    if (description != null && !description.isEmpty()) task.setDescription(description);

                    taskRestService.updateTask(task);

                    if (task.isSubTask()) {
                        Task parent = taskRestService.findById(task.getParentTaskId());
                        LinkedList<Task> subTasks = parent.getSubTasks();
                        subTasks.set(subTasks.indexOf(task), task);
                        parent.setSubTasks(subTasks);
                        taskRestService.updateTask(parent);
                    }

                    responseMessage.setText(Util.displayTask(task) + " " + Emoji.UPDATE);
                } catch (NumberFormatException e) {
                    responseMessage.setText("Task not found. Please give a correct task id.");
                }
            } else {
                responseMessage.setText("Task not found. Please give a correct task id.");
            }
        }
        Util.sendMessageToChat(absSender, responseMessage);
    }
}

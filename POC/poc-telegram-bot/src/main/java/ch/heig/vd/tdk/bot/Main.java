package ch.heig.vd.tdk.bot;

import ch.heig.vd.tdk.bot.config.BotConfig;
import ch.heig.vd.tdk.bot.handlers.CommandsHandler;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

// Example taken from https://github.com/rubenlagus/TelegramBotsExample
@Component
public class Main {
    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new CommandsHandler(BotConfig.getInstance().getBotName()));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
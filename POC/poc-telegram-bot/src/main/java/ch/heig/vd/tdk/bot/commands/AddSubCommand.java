/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : AddSubCommand.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of the /addsub command

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import ch.heig.vd.tdk.bot.services.Emoji;
import ch.heig.vd.tdk.rest.client.dto.Task;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import java.util.*;

public class AddSubCommand extends Command {
    Emoji[] family;

    /**
     * constructor
     * @param iCommandRegistry
     */
    public AddSubCommand(ICommandRegistry iCommandRegistry) {
        super("addsub", "Add a new sub-task to an existing Task", iCommandRegistry);
        family = new Emoji[4];
        family[0] = Emoji.FATHER_DAUGHTER;
        family[1] = Emoji.FATHER_SON;
        family[2] = Emoji.MOTHER_DAUGHTER;
        family[3] = Emoji.MOTHER_SON;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        super.execute(absSender, user, chat, arguments);

        if (arguments.length >= 2) {
            String id = arguments[0];
            if (id.startsWith("#")) id = id.substring(1);

            Object[] array = Util.getDescriptionAndDeadline(Arrays.copyOfRange(arguments, 1, arguments.length));
            String description = (String) array[0];
            Date deadline = (Date) array[1];

            // Get the parent task and add the new task as a subtask
            Task parentTask = taskRestService.findById(id);
            if (parentTask.isSubTask()) {
                responseMessage.setText("A subtask can't have subtasks.");
            } else {
                Task subTask = new Task(description, tdkUser, tdkChat, deadline);
                subTask.setIsSubTask(true);
                subTask.setParentTaskId(id);
                subTask = taskRestService.createTask(subTask);
                LinkedList<Task> taskList = parentTask.getSubTasks();
                taskList.add(subTask);
                parentTask.setSubTasks(taskList);
                taskRestService.updateTask(parentTask);

                if (parentTask != null) {
                    responseMessage.setText(family[new Random().nextInt(family.length)].toString() + " " + Util.displayTask(subTask));
                } else {
                    responseMessage.setText("The parentTask task id is not correct.");
                }
            }
        } else {
            responseMessage.setText("Not enough parameters");
        }
        Util.sendMessageToChat(absSender, responseMessage);
    }
}

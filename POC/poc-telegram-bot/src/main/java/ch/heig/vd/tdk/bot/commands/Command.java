/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : Command.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of a command

 Remark(s)   : This class is a superclass to all commands

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.rest.client.dto.TdkChat;
import ch.heig.vd.tdk.rest.client.dto.TdkUser;
import ch.heig.vd.tdk.rest.client.service.TaskRestService;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

public abstract class Command extends BotCommand {

    protected ICommandRegistry commandRegistry;
    protected TaskRestService taskRestService;

    protected SendMessage responseMessage;
    protected String channelType;
    protected Integer messageId;

    protected TdkUser tdkUser;
    protected TdkChat tdkChat;

    /**
     * constructor
     * @param commandIdentifier the command identifier
     * @param description the command description
     * @param iCommandRegistry the commands handler
     */
    protected Command(String commandIdentifier, String description, ICommandRegistry iCommandRegistry) {
        super(commandIdentifier, description);
        this.commandRegistry = iCommandRegistry;
        this.taskRestService = new TaskRestService();
    }

    @Override
    public void processMessage(AbsSender absSender, Message message, String[] arguments) {
        messageId = message.getMessageId();
        super.processMessage(absSender, message, arguments);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        responseMessage = new SendMessage();
        if (chat.isUserChat())
            responseMessage.setChatId(user.getId().toString());
        else
            responseMessage.setChatId(chat.getId());
        responseMessage.enableHtml(true);
        responseMessage.setReplyToMessageId(messageId);

        tdkUser = new TdkUser(user.getId(), user.getFirstName(), user.getLastName(), user.getUserName(), user.getLanguageCode(), user.getBot());

        if (chat.isChannelChat())
            channelType = TdkChat.CHANNELCHATTYPE;
        else if (chat.isGroupChat())
            channelType = TdkChat.GROUPCHATTYPE;
        else if (chat.isSuperGroupChat())
            channelType = TdkChat.SUPERGROUPCHATTYPE;
        else if (chat.isUserChat())
            channelType = TdkChat.USERCHATTYPE;
        else
            channelType = TdkChat.USERCHATTYPE;

        tdkChat = new TdkChat(chat.getId(), channelType, chat.getTitle());
    }
}

/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : HelpCommand.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of the /help command

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.IBotCommand;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

public class HelpCommand extends Command {

    private final String ADD = "add", ADDSUB = "addsub", CLEAR = "clear", DELETE = "delete", LIST = "list", SETTINGS = "settings", UPDATE = "update", LINE_RETURN = "\n", LINE_JUMP = "\n\n";

    /**
     * constructor
     * @param iCommandRegistry the commands handler
     */
    public HelpCommand(ICommandRegistry iCommandRegistry) {
        super("help", "Get all the ch.heig.vd.tdk.commands this bot provides.", iCommandRegistry);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        responseMessage = new SendMessage();
        responseMessage.setChatId(chat.getId().toString());
        responseMessage.enableHtml(true);
        responseMessage.setReplyToMessageId(messageId);

        String arg;
        if (arguments.length == 0) arg = "";
        else arg = arguments[0];
        arg = arg.toLowerCase();

        switch (arg) {
            case ADD:
                infoAdd();
                break;
            case ADDSUB:
                infoAddSub();
                break;
            case CLEAR:
                infoClear();
                break;
            case DELETE:
                infoDelete();
                break;
            case LIST:
                infoList();
                break;
            case SETTINGS:
                infoSettings();
                break;
            case UPDATE:
                infoUpdate();
                break;
            default:
                infoHelp();
                break;
        }
        Util.sendMessageToChat(absSender, responseMessage);
    }

    /**
     * create the message for the simple help command
     */
    public void infoHelp() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help</b>\n");
        stringBuilder.append("These are the registered ch.heig.vd.tdk.commands for this Bot:").append(LINE_JUMP);

        for (IBotCommand botCommand : commandRegistry.getRegisteredCommands()) {
            stringBuilder.append(botCommand.toString()).append(LINE_JUMP);
        }

        stringBuilder.append("<b>/help cmd</b>").append(LINE_RETURN).append("Give some help for the choosen cmd");
        responseMessage.setText(stringBuilder.toString());
    }

    /**
     * create the message with add as argument
     */
    public void infoAdd() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help for add command:</b>");
        stringBuilder.append(LINE_JUMP)
                .append("<code>/add</code> <b>description</b> dd.mm.yyyy").append(LINE_RETURN).append("Add a task with a deadline").append(LINE_JUMP)
                .append("<code>/add</code> <b>description</b>").append(LINE_RETURN).append("Add a task without deadline").append(LINE_JUMP)
                .append("The description can have multiple words.");
        responseMessage.setText(stringBuilder.toString());
    }

    /**
     * create the message with addsub as argument
     */
    public void infoAddSub() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help for addsub command:</b>");
        stringBuilder.append(LINE_JUMP)
                .append("<code>/addsub</code> <i>#id_parent_task</i> <b>description of sub task</b> dd.mm.yyyy").append(LINE_RETURN).append("Add a sub-task with a deadline to the task with the given id").append(LINE_JUMP)
                .append("<code>/addsub</code> <i>#id_parent_task</i> <b>description of sub task</b>").append(LINE_RETURN).append("Add a sub-task to the task with the given id").append(LINE_JUMP)
                .append("# for id is optional");
        responseMessage.setText(stringBuilder.toString());
    }

    /**
     * create the message with clear as argument
     */
    public void infoClear() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help for clear command:</b>");
        stringBuilder.append(LINE_JUMP).append("<code>/clear</code>").append(LINE_RETURN).append("Delete all existing tasks.");
        responseMessage.setText(stringBuilder.toString());
    }

    /**
     * create the message with delete as argument
     */
    public void infoDelete() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help for delete command:</b>");
        stringBuilder.append(LINE_JUMP)
                .append("<code>/delete</code> <i>#id</i>").append(LINE_RETURN).append("Delete the task with the given id").append(LINE_JUMP)
                .append("# for id is optional");
        responseMessage.setText(stringBuilder.toString());
    }

    /**
     * create the message with list as argument
     */
    public void infoList() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help for list command:</b>");
        stringBuilder.append(LINE_JUMP)
                .append("<code>/list</code>").append(LINE_RETURN).append("Display all tasks").append(LINE_JUMP)
                .append("<code>/list</code> keyword").append(LINE_RETURN).append("Display all tasks containing the keyword");
        responseMessage.setText(stringBuilder.toString());
    }

    /**
     * create the message with settings as argument
     */
    public void infoSettings() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help for settings command:</b>");
        stringBuilder.append(LINE_JUMP)
                .append("<code>/settings</code> <i>reminder</i> <b>days</b> <b>hours:minutes</b>").append(LINE_RETURN).append("Configure the notification time to remind of a task's deadline.\ndays -> how many days before the deadline\nhour:minute -> at what time of the choosen day the notification must appear.").append(LINE_JUMP)
                .append("<code>/settings</code> <i>hide</i> <b>days</b>").append(LINE_RETURN).append("Configure the time when an expired task will automatically be deleted.\ndays -> how many days after the task's stranding, it must be deleted").append(LINE_JUMP)
                .append("<b>days</b> must be an integer").append(LINE_RETURN)
                .append("<b>hours</b> must be an integer between 0 and 23").append(LINE_RETURN)
                .append("<b>minutes</b> must be an integer between 0 and 59").append(LINE_RETURN)
                .append("If you don't want these settings just put a single argument: '-' ");
        responseMessage.setText(stringBuilder.toString());
    }

    /**
     * create the message with update as argument
     */
    public void infoUpdate() {
        StringBuilder stringBuilder = new StringBuilder("<b>Help for update command:</b>");
        stringBuilder.append(LINE_JUMP)
                .append("<code>/update</code> <i>#id</i> <b>new description</b> dd.mm.yyyy").append(LINE_RETURN).append("Update the description and the deadline").append(LINE_JUMP)
                .append("<code>/update</code> <i>#id</i> <b>new description</b>").append(LINE_RETURN).append("Update only the description").append(LINE_JUMP)
                .append("<code>/update</code> <i>#id</i> dd.mm.yyyy").append(LINE_RETURN).append("Update only the deadline").append(LINE_JUMP)
                .append("To delete the deadline, write it as 00.00.0000").append(LINE_RETURN)
                .append("# for id is optional");
        responseMessage.setText(stringBuilder.toString());
    }
}

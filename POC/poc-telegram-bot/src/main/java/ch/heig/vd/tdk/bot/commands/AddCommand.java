/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe 2018 (PDG) - TODOKLM
 File        : AddCommand.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of the /add command

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import ch.heig.vd.tdk.bot.services.Emoji;
import ch.heig.vd.tdk.rest.client.dto.*;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import java.util.Date;


public class AddCommand extends Command {

    /**
     * constructor
     * @param iCommandRegistry the commands handler
     */
    public AddCommand(ICommandRegistry iCommandRegistry) {
        super("add", "Add a new task", iCommandRegistry);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        super.execute(absSender, user, chat, arguments);

        Object[] array = Util.getDescriptionAndDeadline(arguments);
        String description = (String) array[0];
        Date deadline = (Date) array[1];

        if (description.isEmpty()) {
            responseMessage.setText("Please give a description to create a new task");
        } else {
            Task task = new Task(description, tdkUser, tdkChat, deadline);

            try {
                task = taskRestService.createTask(task);
                responseMessage.setText(Util.displayTask(task) + " " + Emoji.NEW.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Util.sendMessageToChat(absSender, responseMessage);
    }
}

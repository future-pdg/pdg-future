package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import ch.heig.vd.tdk.bot.services.Emoji;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.bots.AbsSender;

import java.util.ArrayList;
import java.util.List;

public class SettingsCommand extends Command {
    private final String REMINDER = "reminder", HIDE = "hide", NONE = "-";

    /**
     * constructor
     * @param iCommandRegistry  the commands handler
     */
    public SettingsCommand(ICommandRegistry iCommandRegistry) {
        super("settings", "Configure the bot", iCommandRegistry);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        super.execute(absSender, user, chat, arguments);

        if (arguments.length == 3 && arguments[0].equals(REMINDER)) {
            changeReminder(arguments[1], arguments[2]);
        } else if (arguments.length == 2 && arguments[0].equals(REMINDER)) {
            changeReminder(arguments[1], null);
        } else if (arguments.length == 2 && arguments[0].equals(HIDE)) {
            changeHide(arguments[1]);
        } else {
            responseMessage.setText("Your command is not correct " + Emoji.ANGRY_FACE);
        }
        Util.sendMessageToChat(absSender, responseMessage);
    }

    /**
     * execute the process to change the reminder time
     */
    private void changeReminder(String daysString, String time) {
        int days = 0, hours, minutes;
        if (daysString.equals(NONE)) {
            days = -1;
            responseMessage.setText("The notification reminder has been canceled");
        } else {
            try {
                days = Integer.parseInt(daysString);
                hours = Integer.parseInt(time.substring(0, time.indexOf(":")));
                minutes = Integer.parseInt(time.substring(time.indexOf(":") + 1));
                if (hours > 23 || hours < 0) throw new Exception();
                if (minutes > 59 || minutes < 0) throw new Exception();

                responseMessage.setText("The notification will appear at " + formatTime(hours) + ":" + formatTime(minutes) +
                        ", " + days + " day(s) before the task's deadline.");
            } catch (Exception e) {
                responseMessage.setText("Your arguments are not correct.");
            }
        }

        //save in BD
    }

    /**
     * execute the process to change the hide time
     */
    private void changeHide(String daysString) {
        int days;
        if (daysString.equals(NONE)) {
            days = -1;
            responseMessage.setText("The tasks will never be automatically deleted.");
        } else {
            try {
                days = Integer.parseInt(daysString);
                responseMessage.setText("The tasks will automatically be deleted after " + days + " day(s).");
            } catch (Exception e) {
                responseMessage.setText("Your argument is not correct.");
            }
        }

        //save in BD
    }

    private String formatTime(int time) {
        String str = Integer.toString(time);
        return str.length() == 1 ? "0" + str : str;
    }
}

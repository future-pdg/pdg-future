/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : ListCommand.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation of the /list command

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot.commands;

import ch.heig.vd.tdk.bot.Util;
import ch.heig.vd.tdk.rest.client.dto.Task;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.ICommandRegistry;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class ListCommand extends Command {

    /**
     * constructor
     * @param iCommandRegistry the commands handler
     */
    public ListCommand(ICommandRegistry iCommandRegistry) {
        super("list", "List all tasks", iCommandRegistry);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] arguments) {
        super.execute(absSender, user, chat, arguments);

        try {
            //List<Task> tasks = taskRestService.getTasks(tdkChat, tdkUser);
            List<Task> tasks = new LinkedList<Task>();
            if (arguments.length >= 1)
                tasks = taskRestService.searchTasks(tdkChat, tdkUser, arguments[0]);
            else
                tasks = taskRestService.getTasks(tdkChat, tdkUser);

            StringBuilder response = new StringBuilder();
            if (tasks.isEmpty()) {
                response.append("There is no existing task");
            } else {
                response.append("Here is the list of tasks:\n");
                Stream<Task> tasksStream = tasks.stream();
                tasksStream
                        //.filter(task -> task.getDescription().contains(String.join(" ", arguments)))
                        .sorted(Comparator.comparing(t -> t.getDeadline() == null ? new Date(Long.MIN_VALUE): t.getDeadline()))
                        .forEach(t -> response
                                .append(Util.displayTaskList(t)));
            }

            responseMessage.setText(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Util.sendMessageToChat(absSender, responseMessage);
    }
}

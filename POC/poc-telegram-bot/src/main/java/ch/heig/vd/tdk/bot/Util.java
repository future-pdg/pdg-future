/*
 -----------------------------------------------------------------------------------
 HEIG-VD - Projet De Groupe (PDG) - TODOKLM
 File        : Util.java
 Author(s)   : Amrani Kamil, Nanchen Lionel, Nicole Olivier, Reka Mentor
 Date        : 2018

 Goal        : Implementation the utility class

 Remark(s)   :

 Compiler    : JDK 1.8
 -----------------------------------------------------------------------------------
*/

package ch.heig.vd.tdk.bot;

import ch.heig.vd.tdk.rest.client.dto.Task;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class Util {
    public final static String CLEAR_YES = "c y", CLEAR_NO = "c n";

    /**
     * send the message to the user/chat/channel
     * @param absSender the sender
     * @param message the message to send
     */
    public static void sendMessageToChat(AbsSender absSender, BotApiMethod<?> message) {
        try {
            absSender.execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    /**
     * translate the deadline into a string (display purpose)
     * @param deadline the deadline to translate
     * @return the string containing the deadline translated
     */
    public static String getDeadlineAsString(Date deadline) {
        return deadline == null ? "" : new SimpleDateFormat("dd.MM.yyyy").format(deadline);
    }

    /**
     * translate the task into a string (display purpose)
     * @param task the task to translate
     * @return the string containing the task translated
     */
    public static String displayTask(Task task) {
        StringBuilder display = new StringBuilder();
        String str = "<i>#" + task.getId() + "</i>\t<b>" + task.getDescription() + "</b>\t" + getDeadlineAsString(task.getDeadline());
        display.append(str);
        if (task.getSubTasks() != null) {
            for (Task subTask : task.getSubTasks()) {
                display.append(displaySubTask(subTask));
            }
        }
        return display.toString();
    }

    /**
     * display a task for the list command
     * @param task the task to display
     * @return the string containing the task translated
     */
    public static String displayTaskList(Task task) {
        if(task.isSubTask()) return "";
        else return " - " + displayTask(task) + "\n";
    }

    /**
     * translate the subtask into a string (display purpose)
     * @param task the subtask to translate
     * @return the string containing the subtask translated
     */
    private static String displaySubTask(Task task) {
        return "\n\t\t↳" + "<i>#" + task.getId() + "</i>\t<b>" + task.getDescription() + "</b>\t" + getDeadlineAsString(task.getDeadline());
    }

    /**
     * get the description and the deadline from the command
     * @param arguments the arguments of the command
     */
    public static Object[] getDescriptionAndDeadline(String[] arguments) {
        String description;
        Date deadline = null;
        try {
            deadline = new SimpleDateFormat("dd.MM.yyyy").parse(arguments[arguments.length - 1]);
            description = String.join(" ", Arrays.copyOf(arguments, arguments.length - 1));
        } catch (ParseException e) {
            description = String.join(" ", arguments);
        } catch (Exception e) {
            description = "";
        }
        return new Object[]{description, deadline};
    }
}

# Inline and Custom Keyboard for Telegram

## Custom Keyboard (ReplayKeyboard)

![reply_keyboard](reply_keyboard.png)

To create a custom keyboard you need to use the ReplyKeyboardMarkup class. The following code create the "ABCD" keyboard displayed above.

```java
//Initialize the message that will create the keyboard
SendMessage responseMessage = new SendMessage();
responseMessage.setChatId(chatId);
// Create ReplyKeyboardMarkup object
ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
// Create the keyboard (list of keyboard rows)
List<KeyboardRow> keyboard = new ArrayList<>();
// Create a keyboard row
KeyboardRow rowAB = new KeyboardRow();
// Set each button, you can also use KeyboardButton objects if you need something else than text
rowAB.add("A");
rowAB.add("B");
// Create another keyboard row
KeyboardRow rowCD = new KeyboardRow();
// Set each button for the second line
rowCD.add("C");
rowCD.add("D");
// Add the first row to the keyboard
keyboard.add(rowAB);
// Add the second row to the keyboard
keyboard.add(rowCD);
// Set the keyboard to the markup
keyboardMarkup.setKeyboard(keyboard);
// Add it to the message
responseMessage.setText("The text");
responseMessage.setReplyMarkup(keyboardMarkup);
sendMessageToChat(absSender);
```

To delete a ReplyKeyboardMarkup you need to use a ReplyKeyboardRemove.

```java
ReplyKeyboardRemove remove = new ReplyKeyboardRemove();
responseMessage.setText("The text");
responseMessage.setReplyMarkup(remove); //Add to the message
sendMessageToChat(absSender);
```

## Inline Keyboard

![inline_keyboard](inline_keyboard.png)

To use an inline keyboard you need to use the InlineKeyboardMarkup class. The following code create the second keyboard displayed above (without the URL). More information about URL, callbacks, switch buttons on:  https://core.telegram.org/bots/2-0-intro#callback-buttons.

```java
//Initialize the message that will create the keyboard
SendMessage responseMessage = new SendMessage();
responseMessage.setChatId(chatId);
//Create the keyboard
InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
//Create the button "Get random music"
InlineKeyboardButton upperButton = new InlineKeyboardButton().setText("Get random music").setCallbackData("a_callback_id");
//Create the button "Send music to friends"
InlineKeyboardButton lowerButton = new InlineKeyboardButton().setText("NO").setCallbackData("a_second_callback_id");
//Create the to rows
List<InlineKeyboardButton> upperRow = new ArrayList<>();
List<InlineKeyboardButton> lowerRow = new ArrayList<>();
//Add buttons to rows
upperRow.add(upperButton);
lowerRow.add(lowerButton);
//Create the keyboard
List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
//Add the rows to the keyboard
keyboard.add(upperRow);
keyboard.add(lowerRow);
//Add the keyboard to the InlineKeyboardMarkup
markupInline.setKeyboard(keyboard);
responseMessage.setText("Are you sure?");
//Add the InlineKeyboardMarkup to the message
responseMessage.setReplyMarkup(markupInline);
sendMessageToChat(absSender);
```

##Handle response

The function that handle the response is `CommandHandler::processNonCommandUpdate(Update update)` a function that process all non (registered) command.

For an `InlineKeyboardMarkup` the response is sent as a CallbackQuery.
Code exemple for the clear keyboard message:

```java
public void processNonCommandUpdate(Update update) {
    	//check if the update has a callback query
        if (update.hasCallbackQuery()) {
            //check that the data of the button choosed relate the "yes" "no" clear button.
            //the data is set in the setCallbackData(String) function of the InlineKeyBoardButton
            if (update.getCallbackQuery().getData().startsWith("clear"))
                //call the handler function that is written in the ClearCommand class
                clearCommand.handleClearRespone(update);
        }
    }
```

For a `ReplyKeyBoardMarkup`the response is sent as an normal message.
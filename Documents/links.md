## Telgram's bot

- Général: https://core.telegram.org/bots
- Telegram's API: https://core.telegram.org/bots/api
- Code exemples: https://core.telegram.org/bots/samples
- Bots FAQ: https://core.telegram.org/bots/faq
- API 2.0: https://core.telegram.org/bots/2-0-intro#callback-buttons

### Java SDK:

- https://github.com/rubenlagus/TelegramBots
- https://github.com/pengrad/java-telegram-bot-api

### Tutorial

- https://monsterdeveloper.gitbooks.io/writing-telegram-bots-on-java/content/

### Emoji

- https://apps.timwhitlock.info/emoji/tables/unicode
- https://emojipedia.org/people/

## Frameworks:

### Microsoft Bot Framework:

- https://docs.microsoft.com/en-us/azure/bot-service/bot-service-design-principles?view=azure-bot-service-3.0
- https://tutorials.botsfloor.com/lets-make-a-chatbot-microsoft-bot-framework-node-js-7da211149c2f

    - Lourd à mettre en place

## Botkit:

- https://botkit.ai/:
    - Ne supporte pas Telegram
    + Rapide et facile à mettre en place


## Modules:

### Node.js

- https://github.com/Naltox/telegram-node-bot
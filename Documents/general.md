# General

## Programming language: Java

We choose Java because it is a programming language that we are all familiar with.

## DataBase: MongoDB

- MongoDB permits to keep our reality for our Data Model which contains both relationnal and embedded links.
- The speed of response is important in our case. Our REST API needs to be fast and by looking at benchmark comparaison (which we know all that there are many discussion about their relevance ..) describe how MongoDB is faster than *SQL databases.
- Also, this permits us to test and design with new NoSQL technologies such as MongoDB

## Frameworks

- SDK: https://github.com/rubenlagus/TelegramBots
  which propose more features and has an active community behind.
- Spring Framework for building our REST API and the REST Client API.
  These two parts are the main blocks of our Architecture.
# List of bot commands

Here are the available commands for the TODO bot.

## Create a task

Create a task with an optionnal <end_date>.

**`/add <task_description> [<end_date>]`**

**Examples:**

- **`/add RES Test chap 1, 2 and 3`**
- **`/add RES Test chap 1, 2 and 3 21.01.2018`** 


## Create a sub-task (Related to a task)

**<span class="color:red">ATTENTION:</span>** This command can/will/may change..

Create a sub-task related to a specific task.

**`/addsub #<task_ref> <task_description> [<end_date>]`**

**Examples:**

- **`/addsub #1 RES Test chap 1, 2 and 3 `**
- **`/addsub #1 RES Test chap 1, 2 and 3 21.01.2018`** 

## List

Will show the list of tasks.

**`/list [<keyword_search>]`**

**Examples:**

- **`/list`** will return (and sort tasks by date):

```
#1 - RES Test chap 1,2 and 3         	- 21.01.2018
#2 - PDG demo1   						- 27.01.2018
```

- **`/list test `** will return (and sort tasks by date):

```
Tasks with "test" keywords:

#1 - RES Test chap 1, 2 and 3         - 21.01.2018
```

## Update

Will update a specific task recognized by <task_ref> argument.

**`/update #<task_ref> [<task_description>] [<end_date>]`**

**Examples:**


- **`/update #2 PDG demo2  `** 
- **`/update #2 21.02.2018  `** 

```
Task #2 correctly updated.
#2 - PDG demo2   - 21.01.2018
```

## Delete

Will delete a specific task.

**`/delete <task_ref>`**

**Examples:**

- **`/delete #2`** will return (and sort tasks by date):

```
Task #2 correctly removed
```

## Help

List all command

`/help`

## Settings

The bot comes with a default configuration:

- ```REMINDER_TIME:2``` Send a reminder REMINDER_TIME days before the task end_date
- ```HIDE_AFTER:5``` Hide tasks finished after HIDE_AFTER days

Configure the bot for a specific group or channel like a reminder, etc.


# Iteration plan Demo 3

This document represent the iteration plan for the third demo that will take place on the 24.01.2019

## Bot

- Implement (if possible) the `/action` command
- Implement the `/settings` command (dynamic configuration)

### Other

- Modify the task's id to make them simpler
- Acceptance tests

## Hosting

- Launch our bot on www.producthunt.com
- Make the website


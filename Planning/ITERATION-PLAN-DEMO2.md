# Iteration plan Demo 2

This document represents the iteration plan for the second demo that will take place on the 06.12.2018

## Bot

###New commands

1. Implement `/delete` command
2. Implement `/addsub` command
3. Implement `/update` command

###Enhance commands

1. For command`/list` add the grep functionality
2. Finish `/add` command

###Other

1. Static configuration
2. Unit tests

## Hosting

1. Choose a hosting service
2. Automatic deployment


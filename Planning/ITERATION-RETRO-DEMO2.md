#  Iteration retrospective Demo 2

This document represents the iteration retrospective for the second demo that took place on the 06.12.2018.

## Our retrospective

We implemented some extra features:

- Make every bot message as a response to the user command message.
- To `/help` command we added an argument management, if this argument is the name of an existing command, the bot reply with a message that offer help about this given command.
- When updating a task with a deadline, if the date is 00.00.0000, the deadline is deleted.
- Implemented a `/clear` command that deletes all tasks.

Apart from the static configuration, all the objectives are finished. We changed the idea of a static configuration to a dynamic configuration with the `/settings` command that we will implement in the last demo. We just think that it was pointless to implement a static configuration to replace it later with a dynamic configuration.

## Teacher's retrospective


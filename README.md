# Projet PDG 2018

Auteurs:

 - Amrani Kamil
 - Nanchen Lionel
 - Nicole soit Nicoulaz Olivier
 - Reka Mentor

Ce projet a été réalisé dans le cadre du cours PDG 2018 à la HEIG-VD. 

## TODOKLM c'est quoi ?

TODOKLM c'est un BOT Telegram qui vous permet de gérer vos tâches autant pour vous personnellement que pour un groupe (les tâches sont partagées à tous les membres du groupe). Avec TODOKLM vous pouvez: 

- Lister les tâches
- Créer une tâche (avec ou sans date)
- Créer une sous-tâche
- Modifier une tâche
- Supprimer une tâche
- Supprimer toutes les tâches

Tout cela directement depuis Telegram. 

## Utilisation du BOT

Afin d'utiliser le BOT, ils vous suffit de rechercher le contact TODOKLM et de lui envoyer un message. Vous pouvez dès à présent utiliser les commandes afin d'intéragir avec lui:

- ` /list` vous permet de lister vos tâches.
- `/add DESCRIPTION [DATE - DD.MM.AAAA] ` vous permet d'ajouter une nouvelle tâche.

- ` /addsub [TASK_ID] DESCRIPTION` vous permet d'ajouter une sous tâche à une tâche existante. Le **TASK_ID** représente l'identifiant de la tâche parente (affichée lors du /list)

- `/update  [TASK_ID] [DESCRIPTION] [DATE - DD.MM.AAAA]`  permet de modifier les tâches existantes. Il est possible de supprimer la date d'une tâche en insérant la date suivante: 00.00.000.

- `/delete [TASK_ID]` permet de supprimer une tâche.

- `/clear` permet de supprimer toutes les tâches. Une confirmation est demandée à l'aide de deux boutons.

Exemple d'utilisation des commandes

![presentation](README_images/bot_demo.jpg)

## Installation du projet (faculatif)

Cette étape n'est utile que si vous souhaitez votre propre version du **BOT Telegram** avec les sources disponibles sur Gitlab.

### Prérequis

- Java
- Telegram: https://telegram.org/
- MongoDB:  https://www.mongodb.com/
- Maven: https://maven.apache.org/

### 1. Création du BOT Telegram

La première étape de création de BOT Telegram est de contacter l'utilisateur [BotFather](https://telegram.me/BotFather) sur Telegram qui permet de créer un BOT simplement sur l'application de messagerie. 

Voici un lien sur la documentation officielle qui permet d'en apprendre plus sur ce point : https://core.telegram.org/bots

Voici quelques explications simples sur la création avec BotFather:

```
// commandes à écrire directement au contact BotFather

// demande à BotFather de créer un nouveau bot
/newbot

// donner un nom au bot
TODOKLM

// donner un username au bot (doit terminer par 'bot')
TODOKLMBot
```

Une fois ce point réalisé, vous reçevrez un **TOKEN** qui vous permettra de contacter votre BOT avec votre application. À partir de ce point, vous pouvez déjà écrire à votre BOT sur Telegram mais il ne répondra pas à vos messages / commandes.

### 2. Configuration du BOT Telegram

Afin de configurer votre bot, il suffit d'écrire ` /mybots` à BotFather puis de cliquer sur le bot que vous souhaitez configurer. BotFather vous proposera de configurer votre BOT à l'aide de boutons / messages à écrire:

![BotFather_configuration](README_images/BotFather_config.png)

Prenons l'exemple de la description, cliquez sur "Edit Commands" puis envoyer une liste de vos commandes sous la forme "<commande> - <description> " :

```
start - how to start
help - show help
add - add task
list - list task
addsub - add sub task
delete - delete task
settings - configure the bot
clear - clear all tasks
```

Libre à vous d'ajouter une description, une image etc..

### 3. Récupération des sources du projet

Afin de récupérer les sources du projet, vous pouvez simplement clôner ce repo ou télécharger les sources.

Le projet est séparé en trois parties:

- tdk-rest-client
- tdk-test-api
- poc-telegram-bot

Chaque parties est un projet maven il faudra donc les ouvrir les trois indépendament les uns des autres dans votre éditeur favoris.

Le projet 'tdk-rest-client' est une dépendance du projet 'tdk-rest-api'.

### 4. Configuration de tdk-rest-client

Ouvrez le fichier "app.propoperties" dans le dossier ressources afin de modificer les configurations pour votre environnement. Voici une configuration locale:

```properties
API_URL=127.0.0.1
API_PROTOCOL=http://
API_PORT=8080
API_ROOT_URL=http://127.0.0.1:8080/
```

Ensuite lancez la commande ` mvn clean install` afin de générer les sources. La dépendance est créée dans le dossier .m2 cache de maven afin d'être utilisée dans l'api rest.

### 5. Configuration de tdk-rest-api

Crééez un fichier ".env" dans le dossier ressources où sont demandé les configurations suivantes: (ne pas inclure les lignes commençant par **//** qui sont explicatives). Remplaçez les **xxx**.

```
// configuration de la base de données MongoDB
// host de la base de données. localhost en local.
MONGO_HOST=xxx
// port utilisé par MongoDB (27017 par défaut)
MONGO_PORT=27017
// utilisateur / password de la base de données
MONGO_USER=xxx
MONGO_PASS=xxx
// nom de la base de données
DB_NAME=todoklm
```

Une fois la configuration fonctionnelle et MongoDB fonctionnel, lancez la commande ` mvn clean install`.

### 6. Configuration de poc-telegram-bot

Crééez un fichier ".env" dans le dossier ressources (même chose que pour l'api rest). 

```
// token récupéré au point 1
TELEGRAM_PRIVATE_TOKEN=xxx
// nom du bot donné sur Telegram
BOT_NAME=TODOKLM
```

Une fois la configuration fonctionnelle, lancez la commande ` mvn clean install`. Votre BOT est maintenant fonctionnel et devrait répondre à vos messages sur Telegram.